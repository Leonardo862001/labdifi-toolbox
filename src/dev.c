#include "functions.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
void dev() {
	while ( getchar() != '\n' );
	double* x;
	size_t size = 100;
	size_t N = 0;
	char buffer[100];
	x = (double*)malloc(size * sizeof(double));
	if (x == NULL) goto error;
	printf(
	    "Inserisci Xi, inserisci una riga "
	    "vuota per terminare\n");
	while (1) {
		if (fgets(buffer, (int)sizeof(buffer), stdin) == NULL)
			goto error;
		if(buffer[0]=='\n') break;
		x[N] = atof(buffer);
		N++;
		if (N > 2 * size / 3) {
			size += INC;
			x = (double*)realloc(x, size);
			if (x == NULL) goto error;
		}
	}
	printf("X \u00B1 \u03C3X= %lf \u00B1 %lf\n", mean(x, N), sqrt(variance(x, N)/N));
	free(x);
	return;
error:
	free(x);
	perror("fatal error");
	abort();
}
