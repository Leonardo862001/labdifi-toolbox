#include "functions.h"
double mean(double* x, size_t N) {
	double sum = 0;
	for (size_t i = 0; i < N; i++) {
		sum += x[i];
	}
	return sum / N;
}

double variance(double* x, size_t N) {
	double sum = 0;
	double x_mean = mean(x, N);
	for (size_t i = 0; i < N; i++) {
		sum += SQUARE(x[i] - x_mean);
	}
	return sum / (N - 1);
}
double covariance(double* x, double* y, size_t N) {
	double x_mean = mean(x, N);
	double y_mean = mean(y, N);
	double covariance = 0;
	for (size_t i = 0; i < N; i++) {
		double a = x[i] - x_mean;
		double b = y[i] - y_mean;
		covariance += (a * b) / (N - 1);
	}
	return covariance;
};
