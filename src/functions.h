#include<stdlib.h>
#define INC 100
#define SQUARE(X) ((X) * (X))
double mean(double*  x, size_t N);
double variance(double* x, size_t N);
double covariance(double* x, double* y, size_t N);
