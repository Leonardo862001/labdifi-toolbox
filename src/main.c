#include <stdio.h>
extern void reg_lin();
extern void dev();
int main(){
	int s;
	printf("Inserisci\n1)Per calcolare la deviazione standard\n2)Per calcolare la regressione lineare\n3)Per uscire\n");
	for(;;){
	(void)scanf("%d", &s);
	switch(s){
		case 1:
			dev();
			break;
		case 2:
			reg_lin();
			break;
		case 3:
			return 0;
		default:
			printf("Inserisci un numero valido\n");
	}
	}
	return 0;
}
