#include "functions.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
void reg_lin() {
	while ( getchar() != '\n' );
	double* x;
	double* y;
	double sy;
	size_t size = 100;
	size_t N = 0;
	char buffer[100];
	x = (double*)malloc(size * sizeof(double));
	y = (double*)malloc(size * sizeof(double));
	if (x == NULL || y == NULL) goto error;
	printf(
	    "Inserisci Xi e Yi separati da una virgola, inserisci una riga "
	    "vuota per terminare\n");
	while (1) {
		if (fgets(buffer, (int)sizeof(buffer), stdin) == NULL)
			goto error;
		char* token = strtok(buffer, ",");
		if (token == NULL || *token == '\n') break;
		x[N] = atof(token);
		char* temp = strtok(NULL, ",");
		if (temp == NULL) goto error;
		y[N] = atof(temp);
		N++;
		if (N > 2 * size / 3) {
			size += INC;
			x = (double*)realloc(x, size);
			y = (double*)realloc(y, size);
			if (x == NULL || y == NULL) goto error;
		}
	}
	double sum = 0, sum_of_squares = 0, sum_of_products = 0, sum_of_y = 0,
	       sum_of_y_squared = 0;
	for (size_t i = 0; i < N; i++) {
		sum += x[i];
		sum_of_squares += SQUARE(x[i]);
		sum_of_products += x[i] * y[i];
		sum_of_y += y[i];
		// sum_of_y_squared=SQUARE(y[i]);
	}
	double delta = (double)N * sum_of_squares - SQUARE(sum);
	double q =
	    (double)(sum_of_y * sum_of_squares - sum * sum_of_products) / delta;
	double p = (double)(N * sum_of_products - sum * sum_of_y) / delta;
	double R2 = SQUARE(
	    (covariance(x, y, N) / sqrt(variance(x, N) * variance(y, N))));
	double residuals = 0;
	for(size_t i = 0; i < N; i++){
		residuals += SQUARE(y[i] - (p*x[i]+q));
	}
	double sq = sqrt( ((residuals / delta) * sum_of_squares/(N-2)));
	double sp = sqrt( ((residuals / delta) * N/(N-2)));
	free(x);
	free(y);
	printf("p=%lf q=%lf R^2=%lf\n", p, q, R2);
	printf("\u03C3p=%lf \u03C3q=%lf\n", sp, sq);
	return;
error:
	free(x);
	free(y);
	perror("fatal error");
	abort();
}
